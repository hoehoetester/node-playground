var http = require('http');
var module1 = require('./module1');
var module2 = require('./module2');

// function onRequest(req, response) {
//     response.writeHead(200, { 'Content-Type': 'text/plain' });
//     response.write(module1.myString);
//     response.write(module2.myVariable);
//     module1.myFunction();
//     module2.myFunction();
//     response.end();
// }

// http.createServer(onRequest).listen(8000);

http.createServer(function(req, response) {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.write(module1.myString);
    response.write(module2.myVariable);
    module1.myFunction();
    module2.myFunction();
    response.end();
}).listen(8000);